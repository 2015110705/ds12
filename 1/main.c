/*
2015110705 김민규
본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
/* define tree */
typedef struct node *treePointer;
typedef struct node {
	char		data;		//문자 출력을 위해서 char 형으로 지정
	treePointer leftChild;
	treePointer rightChild;
}tNode;
treePointer root;			//root node

void		inorder(treePointer ptr);
void		preorder(treePointer ptr);
void		postorder(treePointer ptr);
treePointer createNode(char data);
void		createBinTree();

int main()
{
	/* create binary tree */
	printf("creating a binary tree\n\n");
	createBinTree();

	printf("three binary tree traversals\n");
	/* print data inorder */
	printf("inorder traversal\t\t: ");
	inorder(root);
	printf("\n");

	/* print data preorder */
	printf("preorder traversal\t\t: ");
	preorder(root);
	printf("\n");

	/* print data postorder */
	printf("postorder traversal\t\t: ");
	postorder(root);
	printf("\n");

	return 0;
}

void inorder(treePointer ptr)
{
	/* inorder tree traversal */
	if (ptr) {
		inorder(ptr->leftChild);
		printf("%c", ptr->data);
		inorder(ptr->rightChild);
	}
}

void preorder(treePointer ptr)
{
	/* preorder tree traversal */
	if (ptr) {
		printf("%c", ptr->data);
		preorder(ptr->leftChild);
		preorder(ptr->rightChild);
	}
}

void postorder(treePointer ptr)
{
	/* postorder tree traversal */
	if (ptr) {
		postorder(ptr->leftChild);
		postorder(ptr->rightChild);
		printf("%c", ptr->data);
	}
}

/**
 * create node function
 *@param	: data for new node
*/
treePointer createNode(char data)
{
	treePointer newNode = (treePointer)malloc(sizeof(tNode));

	newNode->data = data;
	newNode->leftChild = NULL;
	newNode->rightChild = NULL;

	return newNode;
}

/*
 * create binary tree function
 * Use data A, B, C, D, E
 */
void createBinTree()
{
	/**
	 * make data 
	 A
	  B
	   D E
	  C
	 */
	treePointer temp;
	root = createNode('A');
	root->leftChild = createNode('B');
	root->rightChild = createNode('C');
	
	temp = root;
	temp = temp->leftChild;
	temp->leftChild = createNode('D');
	temp->rightChild = createNode('E');
}