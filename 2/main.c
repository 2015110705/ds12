/*
2015110705 김민규
본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#define MAX_QUEUE_SIZE 100

/* define tree node and declare queue */
typedef struct node *treePointer;
typedef struct node {
	char		data;		//문자 출력을 위해서 char 형으로 지정
	treePointer leftChild;
	treePointer rightChild;
	}tNode;
treePointer root;
treePointer queue[MAX_QUEUE_SIZE];	//linear queue
int			front = -1;
int			rear = -1;

/* traversal function */
void		inorder(treePointer ptr);
void		preorder(treePointer ptr);
void		postorder(treePointer ptr);

/* queue function*/
void		addq(treePointer newNode);
treePointer	deleteq();
void		queueFull();
treePointer deleteEmpty();
treePointer getFront();

/* complete binary tree function */
treePointer createNode(char data);
treePointer createCompBinTree(FILE *fp);
void		insert(treePointer *pRoot, treePointer pNode);
int			hasBothChild(treePointer pNode);

int main()
{
	/* create complete binary tree from input file */
	FILE *fp = fopen("input.txt", "r");
	printf("creating a complete binary tree\n\n");
	createCompBinTree(fp);
	fclose(fp);

	printf("three binary tree traversals\n");
	/* print data inorder */
	printf("inorder traversal\t\t: ");
	inorder(root);
	printf("\n");

	/* print data preorder */
	printf("preorder traversal\t\t: ");
	preorder(root);
	printf("\n");

	/* print data postorder */
	printf("postorder traversal\t\t: ");
	postorder(root);
	printf("\n");

	return 0;
}

void inorder(treePointer ptr)
{
	/* inorder tree traversal */
	if (ptr) {
		inorder(ptr->leftChild);
		printf("%c", ptr->data);
		inorder(ptr->rightChild);
	}
}

void preorder(treePointer ptr)
{
	/* preorder tree traversal */
	if (ptr) {
		printf("%c", ptr->data);
		preorder(ptr->leftChild);
		preorder(ptr->rightChild);
	}
}

void postorder(treePointer ptr)
{
	/* postorder tree traversal */
	if (ptr) {
		postorder(ptr->leftChild);
		postorder(ptr->rightChild);
		printf("%c", ptr->data);
	}
}

/* queue function*/
/**
 * enqueue function
 *@param	: new Node to enqueue
 */
void addq(treePointer newNode)
{
	if(rear >= MAX_QUEUE_SIZE) {
		queueFull();
	}
	rear++;
	queue[rear] = newNode;
}

/**
 * dequeue function
 *@return	: delete element
 */
treePointer deleteq()
{
	/* if queue is empty */
	if(front == rear) {
		return deleteEmpty();
	}
	front++;
	return queue[front];
}

void queueFull()
{
	fprintf(stderr, "Queue is full\n");
	exit(0);
}

treePointer deleteEmpty()
{
	fprintf(stderr, "Queue is Empty\n");
	return NULL;
}

treePointer getFront()
{
	return queue[front + 1];
}

/**
 * create node function
 *@param	: data for new node
 *@return	: treePointer that points new node
 */
treePointer createNode(char data)
{
	treePointer newNode = (treePointer)malloc(sizeof(tNode));

	newNode->data = data;
	newNode->leftChild = NULL;
	newNode->rightChild = NULL;

	return newNode;
}

/**
 * create complete binary tree function
 *@param	: file pointer that has data
 *@return	: 
 */
treePointer createCompBinTree(FILE *fp)
{
	char data;
	while(!feof(fp)) {
		treePointer newNode;
		fscanf(fp, "%c", &data);
		newNode = createNode(data);
		insert(&root, newNode);
	}
}

/**
 * insert node function
 *@param	: root
 *@param	: insert node
 */
void insert(treePointer *pRoot, treePointer pNode)
{
	/* if the tree is empty, initialize the root with new node */
	if(*pRoot == NULL) {
		*pRoot = pNode;
	}
	/* else, get the front node of the queue */
	else {
		treePointer frontNode = getFront();
		
		/* if the left child of this front node doesn't exist*/
		if (frontNode->leftChild == NULL) {
			/* set the left child as the new node */
			frontNode->leftChild = pNode;
		}
		/* else if the right child of this front node doesn't exist */
		else if (frontNode->rightChild == NULL) {
			/* set the right child as the new node */
			frontNode->rightChild = pNode;
		}
		/* if the front node has both the left child and right child*/
		if (hasBothChild(frontNode)) {
			deleteq();
		}
	}
	
	/* Enqueue the new node */
	addq(pNode);
}

/**
 * check if the node has both the left child and right child
 *@param	: node for check
 *@return	: if it has both the left child and right child -> return 1
			  if not, return 0
 */
int	hasBothChild(treePointer pNode)
{
	/* node has both the left child and right child */
	if(pNode->leftChild != NULL && pNode->rightChild != NULL) {
		return 1;
	}
	return 0;
}